package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class SportCar extends Car {
    private int speedLimit;

    public SportCar(String carBrand, String carClass, int weight, int speedLimit, Driver dr, Engine eng) {
        super(carBrand, carClass, weight, dr, eng);
        this.speedLimit = speedLimit;
    }

    public int getSpeedLimit() {
        return speedLimit;
    }

    public void setSpeedLimit(int speedLimit) {
        this.speedLimit = speedLimit;
    }

    @Override
    public String toString() {
        return super.toString() + "; Гранична швидкість: " + speedLimit + " км/год.";

    }
}
