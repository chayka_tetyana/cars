package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Lorry extends Car {
    private int carryingCapacity;

    public Lorry(String carBrand, String carClass, int weight, int carryingCapacity, Driver dr, Engine eng) {
        super(carBrand, carClass, weight, dr, eng);
        this.carryingCapacity = carryingCapacity;
    }

    public int getCarryingCapacity() {
        return carryingCapacity;
    }

    public void setCarryingCapacity(int carryingCapacity) {
        this.carryingCapacity = carryingCapacity;
    }

    @Override
    public String toString() {
        return super.toString() + "; Вантажопідйомність кузова: " + carryingCapacity + " кг.";

    }
}
