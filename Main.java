package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Main {
    public static void main(String[] args) {
        System.out.println("Подивіться на вантажівку!");
        Driver dr1 = new Driver("Яремчук Євген Петрович", 58, "чол.", "0977235765", 35);
        Engine eng1 = new Engine(250, "Renault Trucks");
        letAvtoMove(new Lorry("Renault Premium", "вантажівка", 22500, 10350, dr1, eng1));
        System.out.println("Подивіться на спортивне авто!");
        Driver dr2 = new Driver("Чайка Сергій Олександрович", 34, "чол.", "0967256984", 10);
        Engine eng2 = new Engine(305, "Subaru");
        letAvtoMove(new SportCar("Subaru Impreza", "спортивне", 1400, 244, dr2, eng2));

    }

    public static void letAvtoMove(Car car) {
        System.out.println(car.toString());
        car.start();
        car.turnLeft();
        car.turnRight();
        car.stop();
    }
}
