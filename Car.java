package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public abstract class Car {
    private String carBrand;
    private String carClass;
    private int weight;
    private Driver dr;
    private Engine eng;

    public Car(String carBrand, String carClass, int weight, Driver dr, Engine eng) {
        super();
        this.carBrand = carBrand;
        this.carClass = carClass;
        this.weight = weight;
        this.dr = dr;
        this.eng = eng;
    }

    public Car() {

    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getCarClass() {
        return carClass;
    }

    public void setCarClass(String carClass) {
        this.carClass = carClass;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Driver getDr() {
        return dr;
    }

    public void setDr(Driver dr) {
        this.dr = dr;
    }

    public Engine getEng() {
        return eng;
    }

    public void setEng(Engine eng) {
        this.eng = eng;
    }

    public void start() {
        System.out.println("Поїхали!");
    }

    public void stop() {
        System.out.println("Зупиняємося.");
    }

    public void turnRight() {
        System.out.println("Поворот направо");
    }

    public void turnLeft() {
        System.out.println("Поворот наліво");
    }

    public String toString() {
        return "Автомобіль: " + carBrand + "; Клас: " + carClass + "; Вага: " + weight +
                "кг; " + dr + "; Мотор: " + eng;
    }
}

